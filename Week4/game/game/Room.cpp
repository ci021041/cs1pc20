#include "Room.hpp"

// constructor Room
Room::Room(const std::string& description) : description(description) {}

// function AddItem
void Room::AddItem(const Item& item) {	
	items.push_back(item);
}

// function RemoveItem 
bool Room::RemoveItem(const std::string& item) {
	// iterate over list of items 
	for (auto it = items.begin(); it != items.end(); it++) {
		if (it->GetName() == item) {	// if name matches
			items.erase(it);	// remove respective item 
			return true;
		}
	}
	return false;
}

// function AddExit
void Room::AddExit(const std::string& direction, Room* room) {
	exits[direction] = room;
}

// function GetExit
Room* Room::GetExit(const std::string& direction) {
	auto it = exits.find(direction);
	if (it != exits.end()) {	// iterator not at end (exit found)
		return it->second;	// returns Room* pointer to room that respective exit leads
	}
	else {
		return nullptr;	// exit not found
	}
}

// function SetDescription
void Room::SetDescription(const std::string& desc) {
	description = desc;
}

// function GetDescription
std::string Room::GetDescription() const {
	return description;
}

// function GetItems
std::vector<Item>& Room::GetItems() {
	return items;
}

// function GetExits
std::map<std::string, Room*>& Room::GetExits()  {
	return exits;
}

// functon AddNPC
void Room::AddNPC(const NPC& npc) {
	npcs.push_back(npc);
}

// function GetNPCs
std::vector<NPC>& Room::GetNPCs() {
	return npcs;
}

