#include "NPC.hpp"

NPC::NPC(const std::string& name, const std::string& dialogue) : name(name), dialogue(dialogue) {}

// function GetName
const std::string& NPC::GetName() const {
	return name;
}

// function GetDialogue
const std::string& NPC::GetDialogue() const {
	return dialogue;
}

// function Interact
void NPC::Interact() const {
	std::cout << name << ": " << dialogue << std::endl;
}

