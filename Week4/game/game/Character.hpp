#pragma once
#include <iostream>
#include <string>
#include <vector>
//	include Room.hpp
#include "Room.hpp"

//	create a class Character that represents characters and their stats
class Character {	// inheritance to create different types such as players and monsters
private:
	std::string name;
	int health;
	Room* location;
	int damage;
public:
	Character(const std::string& name, int health, int damage);	// constructor
	//	added functions
	void TakeDamage(int damage);
	void SetLocation(Room* room);
	Room* GetLocation() const;
};

//	create a subclass Player
class Player : public Character {	
public:
	Player(const std::string& name, int health, int damage);
};

// example usage
// Player player1("Alice", 100);
// e.g. player1.SetLocation(&startRoom);	//	set player's starting room