#include "Character.hpp"

// constructor Character
Character::Character(const std::string& name, int health, int damage) : name(name), health(health) , damage(damage){}

//	function TakeDamage
void Character::TakeDamage(int damage) {
	health -= damage;
	if (health < 0) {
		health = 0;
	}
}

// function SetLocation
void Character::SetLocation(Room* room) {
	location = room;
}

// function GetLocation
Room* Character::GetLocation() const {
	return location;
}

//	Player class implementation
Player::Player(const std::string& name, int health, int damage) : Character(name, health, damage) {}

