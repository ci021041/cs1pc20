#pragma once
#include <iostream>
#include <string>

//	create a class Item to represent objects that can be found in rooms or carried by characters
class Item {	// composition within Room and Character classes
private:
	std::string name;
	std::string description;
public:
	Item(const std::string& name, const std::string& description);	// constructor; fixed to description instead of desc
	//	added functions
	std::string GetName() const;
	std::string GetDescription() const;
};

// example usage
// Item key("Key", "A shiny key that looks important.");