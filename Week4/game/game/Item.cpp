#include "Item.hpp"

// constructor Item
Item::Item(const std::string& name, const std::string& description) : name(name), description(description) {}

// function GetName
std::string Item::GetName() const {
	return name;
}

// function GetDescription
std::string Item::GetDescription() const {
	return description;
}

