#pragma once
#include <iostream>
#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
//	include header file for Room
#include "Room.hpp"

//	create a class Area to manage rooms and the connections between them
class Area {
private:
	std::map<std::string, Room*> rooms;
	//  added private function to represent two-way connection
	std::string getOppositeDirection(const std::string& direction);
public:
	void AddRoom(const std::string& name, Room* room);	//	uses name as key to add room to area
	Room* GetRoom(const std::string& name);
	//	connect two rooms using a specified direction (e.g. 'north')
	void ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction);
	void LoadMapFromFile(const std::string& filename);
};

