#pragma once
#include <iostream>
#include <vector>
#include <map>
#include "Room.hpp"
#include "Item.hpp"
#include "Character.hpp"
#include "Area.hpp"
#include "Inventory.hpp"
#include "NPC.hpp"

int main() {
	// create an instance of the Area class
	Area gameWorld;
	// create an instance of Inventory class
	Inventory inventory;
	// Load the game map from a text file
	gameWorld.LoadMapFromFile("game_map.txt");
	//	create items
	Item bingHelmet("Bing helmet", "Legend has it this helmet was stolen from Microsoft Headquarters.");
	Item antiRSIGloves("Anti RSI Gloves", "Guaranteed to prevent RSI for programmers; also packs a punch.");
	Item SRSDocument("SRS Document", "Save Rescue Service Document. Includes details for your main quest.");
	Item mascot("Mascot", "A cute little autonomous GPT-7 robot.");
	Item monsterEnergy("Monster energy", "Deadline in the next 12 hours? Don't worry, Monster energy has your back.");
	Item SSHKey("SSH Key", "Secure Shell Key, it seems you might need it to enter a specific room.");
	Item virus("Virus", "One of the strongest weapons for cyberattacks in the early 21st century.");
	Item enhancingCoffee("Enhancing coffee", "The perfect drink for a programmer.");
	Item rubyStack("Ruby stack", "This could potentially be worth a fortune.");
	Item rubyArmour("Ruby armour", "It seems to reflect light like a mirror.");
	Item signalJammer("Signal jammer", "Releases an EMP to temporarily disable Pat's skill.");
	Item monsterCoffee("Monster coffee", "Borderline drug.");
	// create NPCs
	NPC juniorProgrammer("Junior Programmer", "C and C++ are way too advanced for me. Pointers? What are those?");
	NPC seniorProgrammer("Senior Programmer", "Best programming language? Assembly.");
	NPC robotReceptionist("Robot Receptionist", "Check the SRS Document for the current mission assigned to you.");
	NPC metaverseNPC("Metaverse NPC", "You know what's worse than being an NPC? Being an NPC inside the metaverse inside a game.");
	NPC snakeMan("Snake Man", "Fun fact about snakes: They can help predict earthquakes!");
	NPC coffeeMerchant("Coffee Merchant", "Business has been booming eversince I found out about this place.");
	NPC rubyMaiden("Ruby Maiden", "Leave this room as soon as possible for thou shalt be turned into ruby thyself.");
	NPC patsMinion("Pat's Minion", "Glory to our lord Pat.");
	// set room descriptions and add items
	Room* assemblyHall = gameWorld.GetRoom("Assembly Hall");
	if (assemblyHall != nullptr) {
		assemblyHall->AddItem(SRSDocument);
		assemblyHall->AddItem(mascot);
		assemblyHall->AddItem(monsterEnergy);
		assemblyHall->AddNPC(juniorProgrammer);
		assemblyHall->AddNPC(seniorProgrammer);
		assemblyHall->AddNPC(robotReceptionist);
		assemblyHall->SetDescription("Assembly Hall - You are in a lobby with other programmers who are either conversing, looking at the quest board or claiming their rewards.");
	}
	Room* metaverseHallway = gameWorld.GetRoom("Metaverse Hallway");
	if (metaverseHallway != nullptr) {
		metaverseHallway->AddItem(SSHKey);
		metaverseHallway->AddNPC(metaverseNPC);
		metaverseHallway->SetDescription("Metaverse Hallway - You have been transported to the metaverse. You walk along the path until it splits into two opposite directions.");
	}
	Room* pythonsDen = gameWorld.GetRoom("Python's Den");
	if (pythonsDen != nullptr) {
		pythonsDen->AddItem(virus);
		pythonsDen->AddNPC(snakeMan);
		pythonsDen->SetDescription("Python's Den - You encounter several python hatchlings and a 20-foot Burmese Python in its slumber.");
	}
	Room* javaLand = gameWorld.GetRoom("Java Land");
	if (javaLand != nullptr) {
		javaLand->AddItem(enhancingCoffee);
		javaLand->AddNPC(coffeeMerchant);
		javaLand->SetDescription("Java Land - A coffee-themed area including a coffee waterfall and abnormally large trees made of coffee.");
	}
	Room* rubyRoom = gameWorld.GetRoom("Ruby Room");
	if (rubyRoom != nullptr) {
		rubyRoom->AddItem(rubyStack);
		rubyRoom->AddItem(rubyArmour);
		rubyRoom->AddNPC(rubyMaiden);
		rubyRoom->SetDescription("Ruby Room - A treasure room filled with rubies of various sizes, beaming in bright red.");
	}
	Room* oopRoom = gameWorld.GetRoom("OOP Room");
	if (oopRoom != nullptr) {
		oopRoom->AddItem(signalJammer);
		oopRoom->AddItem(monsterCoffee);
		oopRoom->AddNPC(patsMinion);
		oopRoom->SetDescription("OOP Room - The Optimus Omnipotent Pat Room, with the final boss Pat lighting a cigarette while writing lines of code using psychokinesis. You realise the rumours of Pat turning into a cyborg is true.");
	}
	Room* theSimulation = gameWorld.GetRoom("The Simulation");
	if (theSimulation != nullptr) {
		theSimulation->SetDescription("The Simulation - You come to the realisation that reality is indeed a simulation controlled by 'The Anonymous'. An unknown being instantly annihilates you. Game Over");
	}
	//	create a player
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::string playerName;
	std::cout << "Enter your name: ";
	std::getline(std::cin, playerName);
	Player player(playerName, 100, 10);
	std::cout << "Welcome " << playerName << ", to the game 'Life of a Programmer'! It is the year 2100. Programmers are now the saviours of the world in the war against AI. You have embarked on your journey to become a programming legend like your role model, Pat Parslow." << std::endl;
	//	set the player's starting location
	Room* currentRoom = assemblyHall;
	player.SetLocation(currentRoom);
	inventory.AddItem(bingHelmet);
	inventory.AddItem(antiRSIGloves);
	// create mobs
	Character markZuckerburg("Mark Zuckerburg", 15, 2);
	Character blackHatchling("Black Hatchling", 5, 1);
	Character whiteHatchling("White Hatchling", 5, 1);
	Character burmesePython("Burmese Python", 20, 3);
	Character homelessDropout("Homeless Dropout", 5, 1);
	Character caffeinatedDropout("Caffeinated Dropout", 15, 2);
	Character rubyAI("Ruby AI", 5, 1);
	Character rubyAGI("Ruby AGI", 15, 2);
	Character patParslow("Pat Parslow", 50, 5);
	// set character's locations
	markZuckerburg.SetLocation(metaverseHallway);
	blackHatchling.SetLocation(pythonsDen);
	whiteHatchling.SetLocation(pythonsDen);
	burmesePython.SetLocation(pythonsDen);
	homelessDropout.SetLocation(javaLand);
	caffeinatedDropout.SetLocation(javaLand);
	rubyAI.SetLocation(rubyRoom);
	rubyAGI.SetLocation(rubyRoom);
	patParslow.SetLocation(oopRoom);
	// Game loop 
	while (true) {
		/*cout << "> ";
		getline(cin, command);
		if (command == "quit") {
			break;
		}
		interpreter.interpretCommand(command);
		*/
		//	informs user of player's current location and items in the room
		Room* currentRoom = player.GetLocation();
		if (currentRoom != nullptr) {
			std::cout << "Current Location: " << currentRoom->GetDescription() << std::endl;
		}
		//	gives user options to choose
		std::cout << "Options: ";
		std::cout << "1. Look around | ";
		std::cout << "2. Interact with an item | ";
		//	added function to open inventory
		std::cout << "3. Open inventory | ";
		std::cout << "4. Move to another room | ";
		std::cout << "5. Quit" << std::endl;
		//	receives user input from 1-5 as choices
		//	if conditionals depending on user's choice 
		int choice;
		std::cin >> choice;
		if (choice == 1) {
			// Player looks around (no action required)
			std::cout << "You look around the room." << std::endl;
			std::cout << "Items in the room:" << std::endl;
			for (const Item& item : currentRoom->GetItems()) {
				std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
			}
			std::cout << std::endl;
			std::cout << "Exits available : ";
			for (const auto& exit : currentRoom->GetExits()) {
				std::cout << exit.first << " ";
			}
			std::cout << std::endl;
			std::cout << "What would you like to do? ";
			std::cout << "Options: ";
			std::cout << "1. Pick up an item | ";
			std::cout << "2. Talk to someone | ";
			std::cout << "3. Previous options" << std::endl;
			int choice2;
			std::cin >> choice2;
			if (choice2 == 1) {
				std::cout << "Which item would you like to pick up? ";
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');	// to allow for spaces 
				std::string itemName;	
				std::getline(std::cin, itemName);	// user input stored in string itemName
				for (const Item& item : currentRoom->GetItems()) {	// iterates through all the items in currentRoom (pointer to player.GetLocation)
					if (item.GetName() == itemName) {	// if user input matches
						inventory.AddItem(item);	// item added to inventory
						currentRoom->RemoveItem(itemName);	// item removed from currentRoom
						std::cout << "You have picked up " << itemName << "." << std::endl;	// informs user that item has been picked up
						break;
					}
				}
			}
			else if (choice2 == 2) {
				std::cout << "Who would you like to talk to?" << std::endl;
				for (const NPC& npc : currentRoom->GetNPCs()) {
					std::cout << "- " << npc.GetName() << std::endl;
				}
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				std::string npcName;
				std::getline(std::cin, npcName);
				for (const NPC& npc : currentRoom->GetNPCs()) {
					if (npc.GetName() == npcName) {
						npc.Interact();
						break;
					}
				}
			}
		}
		else if (choice == 2) {
			// Player interacts with an item in the room
			std::cout << "Which item would you like to use? ";
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::string itemName;
			std::getline(std::cin, itemName);
			inventory.UseItem(itemName);
		}
		else if (choice == 3) {
			// Opens inventory
			inventory.ListItems();
			std::cout << std::endl;
		}
		else if (choice == 4) {
			// Player moves to another room
			std::cout << "Enter the direction north, south, east or west: ";
			std::string direction;
			std::cin >> direction;
			Room* nextRoom = currentRoom->GetExit(direction);
			if (nextRoom != nullptr) {
				player.SetLocation(nextRoom);
				std::cout << "You move to the next room." << std::endl;
				currentRoom = nextRoom;
			}
			else {
				std::cout << "You can't go that way." << std::endl;
			}
		}
		else if (choice == 5) {
			// Quit the game
			std::cout << "Goodbye!" << std::endl;
			break;
		}
		else {
			std::cout << "Invalid choice. Try again." << std::endl;
		}
	}
	return 0;
}


