#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
//	include header file for Item
#include "Item.hpp"
#include "NPC.hpp"

//	create a class Room to represent location
class Room {	// composition
private:
	std::string description;
	std::map <std::string,Room*> exits;	//	maps directions to other Room objects (e.g. north, south)
	std::vector <Item> items;	//	vector to hold Item objects in room
	std::vector <NPC> npcs;
public:
	//	functions
	Room(const std::string& description);
	void AddItem(const Item& item);	
	bool RemoveItem(const std::string& item);	
	//	added functions
	void AddExit(const std::string& direction, Room* room);
	Room* GetExit(const std::string& direction);
	//const std::string& GetName() const;
	void SetDescription(const std::string& desc);
	std::string GetDescription() const;
	std::vector<Item>& GetItems();
	std::map<std::string, Room*>& GetExits();
	void AddNPC(const NPC& npc);
	std::vector<NPC>& GetNPCs();
};

// example usage
// Room startRoom("You are in a dimly lit room.");