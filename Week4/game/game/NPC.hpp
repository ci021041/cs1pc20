#pragma once
#include <iostream>
#include <string>
#include <vector>

//	Dynamic NPC Framework
class NPC {
private:
	std::string name;
	std::string dialogue;
public:
	NPC(const std::string& name, const std::string& dialogue);
	const std::string& GetName() const;
	const std::string& GetDialogue() const;
	void Interact() const;
};