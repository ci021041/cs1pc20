#include "Inventory.hpp"

void Inventory::AddItem(const Item& item) {
	items.push_back(item);
}

bool Inventory::RemoveItem(const std::string& itemName) {
	for (auto it = items.begin(); it != items.end(); it++) {
		if (it->GetName() == itemName) {
			items.erase(it);
			return true;	// item found in inventory and removed
		}
	}
	return false;	// not found 
}

// function ListItems
// only prints out items in inventory
void Inventory::ListItems() const {
	if (items.empty()) {
		std::cout << "Inventory is empty." << std::endl;
		return;
	}
	std::cout << "Inventory items:" << std::endl;
	for (const auto& item : items) {	// iterate over each item in items vector	
		// prints out item's name and description using get functions
		std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
	}
}

// function UseItem
// item disappears from inventory 
void Inventory::UseItem(const std::string& itemName) {
	for (auto& item : items) {
		if (item.GetName() == itemName) {
			std::cout << "Using '" << itemName << "'." << std::endl;
			RemoveItem(itemName);
			return;
		}
	}
	std::cout << "Item '" << itemName << "' not found in inventory." << std::endl;
}

