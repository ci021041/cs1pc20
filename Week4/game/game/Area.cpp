#pragma once
#include "Area.hpp"

//	function AddRoom
void Area::AddRoom(const std::string& name, Room* room) {
	rooms[name] = room;
}

//	function GetRoom
Room* Area::GetRoom(const std::string& name) {
	auto it = rooms.find(name);
	if (it != rooms.end()) {
		return it->second;
	}
	else {
		return nullptr;	// room not found
	}
}

// function getOppositeDirection
// returns opposite direction to reduce duplicate room connections in game_map.txt
// e.g. if Assembly Hall|Metaverse Hallway|north is in game_map.txt
// additional line Metaverse Hallway|Assembly Hall|south is not required
std::string Area::getOppositeDirection(const std::string& direction) {
	if (direction == "north") return "south";
	if (direction == "south") return "north";
	if (direction == "east") return "west";
	if (direction == "west") return "east";
	return "";
}

//	function ConnectRooms
void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
	// initialise room1 and room2 using GetRoom function
	Room* room1 = GetRoom(room1Name);
	Room* room2 = GetRoom(room2Name);
	if (room1 && room2) {
		room1->AddExit(direction, room2);
		std::string oppositeDirection = getOppositeDirection(direction);
		if (!oppositeDirection.empty()) {	// check if oppositeDirection is not empty
			room2->AddExit(oppositeDirection, room1);	// adds exit from room2 back to room1
		}
	}
}

//  LoadMapFromFile function implementation
void Area::LoadMapFromFile(const std::string& filename) {
	std::ifstream file(filename);	// open file and check if successfully opened
	if (file.is_open()) {	// proceeds upon successfully opening file
		std::string line;
		while (std::getline(file, line)) {	// read file line by line
			std::istringstream iss(line);	// split line into three parts at '|'
			std::string room1Name, room2Name, direction;
			if (std::getline(iss, room1Name, '|') && std::getline(iss, room2Name, '|') && std::getline(iss, direction)) {
				// add room1 and room2 to the game
				if (GetRoom(room1Name) == nullptr) {
					Room* room1 = new Room(room1Name);
					AddRoom(room1Name, room1);
				}
				if (GetRoom(room2Name) == nullptr) {
					Room* room2 = new Room(room2Name);
					AddRoom(room2Name, room2);
				}
				ConnectRooms(room1Name, room2Name, direction);
			}
		}
		file.close();	// close file
	}
	else {
		std::cout << "Failed to open file: " << filename << std::endl;
	}
}
