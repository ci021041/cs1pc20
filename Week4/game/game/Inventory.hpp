#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Item.hpp"

class Inventory {
private:
	std::vector<Item> items;	//	Stores items
public:
	void AddItem(const Item& item);
	bool RemoveItem(const std::string& itemName);	// Remove item by name
	void ListItems() const;
	void UseItem(const std::string& itemName);
};
