#include <iostream>
#include <assert.h>
using namespace std;
// Function to be completed
float calculateRatio(float a, float b) {
	return a / b;
}

// Function to perform tests
void runTests() {
	float result1 = calculateRatio(5.0, 2.0);
	assert(result1 == 2.5);
	float result2 = calculateRatio(-10.0, 4.0);
	assert(result2 == -2.5);
}

int main() {
	runTests();
	cout << ("All tests passed successfully!\n");
	return 0;
}
