#include <iostream>
#include <assert.h>
using namespace std;
// Function to be completed
int calculateQuotient(int a, int b) {
	return a / b;
}

// Function to perform tests
void runTests() {
	int result1 = calculateQuotient(10, 3);
	assert(result1 == 3);
	int result2 = calculateQuotient(-15, 5);
	assert(result2 == -3);
}

int main() {
	runTests();
	printf("All tests passed sucessfully!\n");
	return 0;
}
