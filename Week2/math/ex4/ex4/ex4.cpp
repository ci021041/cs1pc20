#include <iostream>
#include <assert.h>
#include <limits.h>
using namespace std;
// Function to be completed
int multiplyIntegers(int a, int b) {
	return a * b;
}

void runTests() {
	int result1 = multiplyIntegers(5, 10);
	assert(result1 == 50);
	int result2 = multiplyIntegers(-7, 8);
	assert(result2 == -56);
	// Test with values that may lead to overflow
	int value1 = INT_MIN / 2;
	int value2 = 2;
	int result3 = multiplyIntegers(value1, value2);
	cout << result3 << endl;
	cout << INT_MIN << endl;
	assert(result3 == INT_MIN);
}

int main() {
	runTests();
	cout << ("All tests passed successfully!\n");
	return 0;
}
