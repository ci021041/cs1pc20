#include <iostream>
#include <assert.h>
using namespace std;
// Function to be completed
int calculateRemainder(int a, int b) {
	return a % b;
}

// Function to perform tests
void runTests() {
	int result1 = calculateRemainder(10, 3);
	assert(result1 == 1);
	int result2 = calculateRemainder(-15, 5);
	assert(result2 == 0);
}

int main() {
	runTests();
	printf("All tests passed successfully!\n");
	return 0;
}
