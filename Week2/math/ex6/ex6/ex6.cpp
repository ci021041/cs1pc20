#include <iostream>
#include <assert.h>
using namespace std;
// Function to be completed
float multiplyFloats(float a, float b) {
    return a * b;
}

// Function to perform tests
void runTests() {
    float result1 = multiplyFloats(2.5, 3.0);
    assert(result1 == 7.5);
    float result2 = multiplyFloats(-1.5, 4.0);
    assert(result2 == -6.0);
}

int main() {
    runTests();
    cout << ("All tests passed successfully!\n");
    return 0;
}
