#include <iostream>
#include <assert.h>
using namespace std;
// Function to be completed
int calculateSum(int a, int b) {
    // Hint: Choose the correct operator to calculate the sum
    // Options: '+', '-', '*', '/'
    // Your code here
    return a + b;
    // Hint: Don't forget to return the calculated sum
    // Your code here
}

// Function to perform tests
void runTests() {
    int result1 = calculateSum(5, 3);
    assert(result1 == 8);

    int result2 = calculateSum(-2, 8);
    assert(result2 == 6);

    // Add more test cases if needed
}

int main() {
    // Call the test function
    runTests();

    cout << ("All tests passed successfully!\n");

    return 0;
}

